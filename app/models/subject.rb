class Subject < ApplicationRecord
  has_many :grades
  def to_s
    name
  end
end
