class Student < ApplicationRecord
  has_many :grades
  def age
    ((Date.today - self.birth_date)/365).to_i
  end
  
  def to_s
    name + " " + surname
  end
end
