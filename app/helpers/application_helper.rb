module ApplicationHelper
  def style_active(path)
    return "active" if (request.path == path)
  end
  def menu_item(body, url)
    link = link_to body, url, class: ["nav-link",style_active(url)]
    content_tag :li, link, class: "nav-item"
  end
end
