Rails.application.routes.draw do
  devise_for :users
  resources :grades
  resources :subjects
  resources :students
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
