#!/usr/bin/env ruby
def fib(index)
  return 0 if index == 1
  return 1 if index == 2
  prevprev = 0
  prev = 1
  actual = 0
  for i in 3..index
    actual = prev + prevprev
    prevprev = prev
    prev = actual
  end
  return actual
end

for i in 1..10000
  puts fib(i)
end

