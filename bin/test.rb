#!/usr/bin/env ruby
prevprev = 0
prev = 1
for i in 1..10000
  if i == 1
    puts 0
  elsif i == 2
    puts 1
  else
    actual = prev + prevprev
    prevprev = prev
    prev = actual
    puts actual
  end
end
	
